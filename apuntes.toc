\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {spanish}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Primer Parcial}{5}{chapter.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Grupos}{5}{section.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Subgrupos}{16}{section.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Clases laterales}{17}{section*.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Afirmación 1.}{17}{section*.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Afirmación 2.}{18}{section*.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Afirmación 3.}{18}{section*.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Afirmación 4.}{18}{section*.6}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Afirmación 5.}{18}{section*.7}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Afirmación 1.}{20}{section*.9}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Afirmación 2}{20}{section*.10}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Afirmación 1}{21}{section*.12}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Grupos Cíclicos}{22}{section.1.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Existencia}{23}{section*.13}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Unicidad}{24}{section*.14}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Segundo Parcial}{27}{chapter.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Subgrupos Normales}{27}{section.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Grupos Cocientes}{28}{section.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Homomorfismos}{30}{section.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Teoremas de Isomorfismos}{34}{subsection.2.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Automorfismos}{36}{subsection.2.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Acciones de Grupo}{38}{section.2.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Tercer Parcial}{43}{chapter.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Teoremas de Sylow}{43}{section.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Producto Directo de Grupos}{48}{section.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Grupo Simétrico}{49}{section.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{Existencia}{50}{section*.15}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Grupos Abelianos Finitos y Finitamente Generados}{56}{section.3.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}Grupos Solubles y Nilpotentes}{59}{section.3.5}% 
